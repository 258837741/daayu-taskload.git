<?php

namespace app\admin\controller;

use daayu\taskload\TaskLoadService;

class TaskLoad extends Base
{
    private $service;
    public function __construct()
    {
        $this->service = new TaskLoadService(config('task_load.db_connection', ''));
    }

    /**
     * 任务列表
     *
     * @return \think\Response
     */
    public function taskList()
    {
        $args['project_id'] = input('get.project_id/d', 0);
        $args['server_id'] = input('get.server_id/d', 0);
        $projects = $this->service->listProjectName();
        return view('', ['projects'=>$projects, 'args'=>$args]);
    }

    public function getTaskList(){

        $re = [
            'code' => 0,
            'msg' => '请求成功',
            'data' => []
        ];
        $args = [];
        $page = input('page/d', 1);
        $rows = input('rows/d', config('config.page_size'));
        $sort_field  = input('sort_field/s', '');
        $sort_type = input('sort_type/s', '');
        $keytype=input('get.keytype/d',0);
        $keyword=trim(input('get.keyword/s',''));
        if(!empty($keyword)){
            switch ($keytype) {
                case 1:
                    $args['name'] = $keyword;
                    break;
                case 2:
                    $args['command'] = $keyword;
                    break;
            }
        }
        $status = trim(input('get.status/s', ''));
        if($status !== ''){
            $args['status'] = $status;
        }
        $command_type = trim(input('get.command_type/s', ''));
        if($command_type !== ''){
            $args['command_type'] = $command_type;
        }
        $project_id = trim(input('get.project_id/s', ''));
        if($project_id !== ''){
            $args['project_id'] = $project_id;
        }
        $server_id = trim(input('get.server_id/s', ''));
        if($server_id !== ''){
            $args['server_id'] = $server_id;
        }
        if (!empty($sort_field) && !empty($sort_type)) {
            $args['order_by'] = [$sort_field=>$sort_type];
        }
        $args['page'] = $page;
        $args['rows'] = $rows;
        $rs=$this->service->listTask($args);
        if (!$rs) {
            $re['code'] = -1;
            $re['msg'] = $this->service->getError();
        }
        $re['data'] = $rs['rows'];
        return json(re_data($re['code'], $re['msg'], $re['data'], $page, $rows, $rs['total']));

    }

    public function taskDetail(){
        $id = input('get.id/d', 0);
        if($id){
            $info = $this->service->getTask($id);
        }else{
            $info = [];
        }
        $projects = $this->service->listProjectName();
        return view('', ['info' => $info, 'projects'=>$projects]);
    }

    public function saveTask(){
        $data = input('post.');
        if(!$data['id']) unset($data['id']);
        $rs = $this->service->saveTask($data);
        if(!$rs){
            return json(re_data(1,'保存失败:'.$this->service->getError()));
        }
        return json(re_data(0, '保存成功'));
    }

    public function deleteTask(){
        $id = input('post.id/d', 0);
        if($id <= 0){
            return json(re_data(1,'参数错误'));
        }
        $rs = $this->service->deleteTask($id);
        if(!$rs){
            return json(re_data(1,'删除失败:'.$this->service->getError()));
        }
        return json(re_data(0, '删除成功'));
    }

    public function serverTask(){
        $id = input('get.id/d', 0);
        if($id <= 0){
            return json(re_data(1,'参数错误'));
        }
        $info = $this->service->getTask($id);
        $serverTasks = $this->service->listServerTask(['server_id'=>$id]);
        // var_dump($serverTasks);die;
        return view('', ['info'=>$info,'serverTasks'=>$serverTasks]);
    }
    
    public function saveServerTask(){
        $id = input('post.id/d', 0);
        $task_ids = input('post.task_ids/s', '');
        if($id <= 0){
            return json(re_data(1,'参数错误'));
        }
        $rs = $this->service->updateServerTasks($id, $task_ids);
        if(!$rs){
            return json(re_data(1,'保存失败:'.$this->service->getError()));
        }
        return json(re_data(0, '保存成功'));
    }

    /**
     * 服务器列表
     *
     * @return \think\Response
     */
    public function projectList()
    {
        return view();
    }

    public function getProjectList()
    {
        $re = [
            'code' => 0,
            'msg' => '请求成功',
            'data' => []
        ];
        $args = [];
        $page = input('page/d', 1);
        $rows = input('rows/d', config('config.page_size'));
        $sort_field  = input('sort_field/s', '');
        $sort_type = input('sort_type/s', '');
        $keytype=input('get.keytype/d',0);
        $keyword=trim(input('get.keyword/s',''));
        if(!empty($keyword)){
            switch ($keytype) {
                case 1:
                    $args['name'] = $keyword;
                    break;
                case 2:
                    $args['command'] = $keyword;
                    break;
            }
        }
        $status = trim(input('get.status/s', ''));
        if($status !== ''){
            $args['status'] = $status;
        }
        if (!empty($sort_field) && !empty($sort_type)) {
            $args['order_by'] = [$sort_field=>$sort_type];
        }
        $args['page'] = $page;
        $args['rows'] = $rows;
        $rs=$this->service->listProject($args);
        if (!$rs) {
            $re['code'] = -1;
            $re['msg'] = $this->service->getError();
        }
        $re['data'] = $rs['rows'];
        return json(re_data($re['code'], $re['msg'], $re['data'], $page, $rows, $rs['total']));

    }

    public function projectDetail(){
        $id = input('get.id/d', 0);
        if($id){
            $info = $this->service->getProject($id);
        }else{
            $info = [];
        }
        return view('', ['info'=>$info]);
    }

    public function saveProject(){
        $data = input('post.');
        if(!$data['id']) unset($data['id']);
        $rs = $this->service->saveProject($data);
        if(!$rs){
            return json(re_data(1,'保存失败:'.$this->service->getError()));
        }
        return json(re_data(0, '保存成功'));
    }

    public function deleteProject(){
        $id = input('post.id/d', 0);
        if($id <= 0){
            return json(re_data(1,'参数错误'));
        }
        $rs = $this->service->deleteProject($id);
        if(!$rs){
            return json(re_data(1,'删除失败:'.$this->service->getError()));
        }
        return json(re_data(0, '删除成功'));
    }

    /**
     * 服务器列表
     *
     * @return \think\Response
     */
    public function serverList()
    {
        return view();
    }

    public function getServerList()
    {
        $re = [
            'code' => 0,
            'msg' => '请求成功',
            'data' => []
        ];
        $args = [];
        $page = input('page/d', 1);
        $rows = input('rows/d', config('config.page_size'));
        $sort_field  = input('sort_field/s', '');
        $sort_type = input('sort_type/s', '');
        $keytype=input('get.keytype/d',0);
        $keyword=trim(input('get.keyword/s',''));
        if(!empty($keyword)){
            switch ($keytype) {
                case 1:
                    $args['name'] = $keyword;
                    break;
                case 2:
                    $args['command'] = $keyword;
                    break;
            }
        }
        $status = trim(input('get.status/s', ''));
        if($status !== ''){
            $args['status'] = $status;
        }
        if (!empty($sort_field) && !empty($sort_type)) {
            $args['order_by'] = [$sort_field=>$sort_type];
        }
        $args['page'] = $page;
        $args['rows'] = $rows;
        $rs=$this->service->listServer($args);
        if (!$rs) {
            $re['code'] = -1;
            $re['msg'] = $this->service->getError();
        }
        $re['data'] = $rs['rows'];
        return json(re_data($re['code'], $re['msg'], $re['data'], $page, $rows, $rs['total']));

    }

    public function serverDetail(){
        $id = input('get.id/d', 0);
        if($id){
            $info = $this->service->getServer($id);
        }else{
            $info = [];
        }
        return view('', ['info'=>$info]);
    }

    public function saveServer(){
        $data = input('post.');
        if(!$data['id']) unset($data['id']);
        $rs = $this->service->saveServer($data);
        if(!$rs){
            return json(re_data(1,'保存失败:'.$this->service->getError()));
        }
        return json(re_data(0, '保存成功'));
    }

    public function deleteServer(){
        $id = input('post.id/d', 0);
        if($id <= 0){
            return json(re_data(1,'参数错误'));
        }
        $rs = $this->service->deleteServer($id);
        if(!$rs){
            return json(re_data(1,'删除失败:'.$this->service->getError()));
        }
        return json(re_data(0, '删除成功'));
    }

}
