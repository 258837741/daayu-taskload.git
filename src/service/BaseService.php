<?php
declare (strict_types = 1);

namespace daayu\taskload\service;

use daayu\taskload\TaskLoadConfig;
use think\facade\Db;

/**
 * BaseService层
 */
class BaseService
{
    protected $connection = null;
    protected $table = '';
    protected $error = null;
    protected $command_type_names = ['', 'think', 'curl', 'bash', 'python', 'php'];
    protected $status_names = ['禁用', '启用'];

    public function getError()
    {
        return $this->error;
    }

    /**
     * 构造方法
     */
    public function __construct($connection = null)
    {
        if($connection){
            $this->connection = $connection;
        }else{
            $this->connection = TaskLoadConfig::DEFAULT_CONNECTION;
        }
    }

    protected function db()
    {
        return Db::connect($this->connection);
    }

    protected function table($table = null)
    {
        return $this->db()->table(empty($table) ? $this->table : $table);
    }

    /**
     * 检查数据表是否存在
     */
    protected function checkTable($table = null)
    {
        $table = empty($table) ? $this->table : $table;
        $r = $this->db()->query("SELECT table_name FROM information_schema.TABLES WHERE table_name ='$table'");
        return empty($r) ? false : true;
    }
}
